//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW5
//gMatrix.hpp, implement of matrix class

#include "gMatrix.h"

using namespace std;

const int MAX_ITERS = 1000000;
const auto MIN_DIFF = 0.001;


template<typename T>
T& gMatrix<T>::element(const int i, const int j)
{
    if(valid(i,j))
        return m_data[i][j];
    else
        throw std::logic_error("invalid access");
}

template<typename T>
bool gMatrix<T>::valid(const int i, const int j) const
{
    //check if both are nonnegative
    bool result = i >= 0 && j >= 0;

    //check if both are less than respective dimension
    result = result && i < m_xsz && j < m_ysz;

    return result;
}

template<typename T>
bool gMatrix<T>::samesz(const gMatrix<T> & mRHS) const
{
    //compare both dimensions
    return (ysz() == mRHS.ysz()) && (xsz() == mRHS.xsz());
}

template<typename T>
gMatrix<T>::gMatrix(const int i, const int j)
{
    //initalize size
    m_ysz = 0;
    m_xsz = 0;

    //use resize function to allocate
    resize(i ,j);
}


template<typename T>
void gMatrix<T>::resize(const int i, const int j)
{
    //if either is negative
    if(i < 0 || j < 0)
        throw std::logic_error("invalid matrix size");

    //if just one is zero
    if(i == 0 && j != 0)
        throw std::logic_error("invalid matrix size");

    //if just one is zero
    if(i != 0 && j == 0)
        throw std::logic_error("invalid matrix size");


    //if data already allocated
    //e.g., size not zero
    if(xsz() != 0 && ysz() != 0)
    {
        //2d array dellocation
        for(int k = 0; k < xsz(); k++)
        {
            if(ysz() > 1)
                delete [] m_data[k];
            else
                delete [] m_data[k];
        }
        
        if(xsz() > 1)
            delete [] m_data;
        else
            delete [] m_data;

        //update size
        m_xsz = 0;
        m_ysz = 0;
    }

    //if allocation size is nonzero
    if(i != 0 && j != 0)
    {
        m_xsz = i;
        m_data = new T* [m_xsz];
        for(int k = 0; k < i; k++)
            m_data[k] = new T [j];
        m_ysz = j;
    }

}

template<typename T>
gMatrix<T> & gMatrix<T>::operator=(const gMatrix<T> & mRHS)
{
    resize(mRHS.xsz(), mRHS.ysz());

    //copy all elements
    for(int i = 0; i < xsz(); i++)
        for(int j = 0; j < ysz(); j++)
            element(i,j) = mRHS.get(i,j);

    return *this;
}

template<typename T>
gMatrix<T>::gMatrix(const gMatrix<T>& mRHS)
{
    m_xsz = 0;
    m_ysz = 0;

    *this = mRHS;
}

template<typename T>
gMatrix<T> & gMatrix<T>::operator*=(const gMatrix<T> & mRHS)
{
    *this = *this * mRHS;
}

template<typename T>
gMatrix<T> gMatrix<T>::operator*(const gMatrix<T> & mRHS) const
{
    if(xsz() != mRHS.ysz())
        throw std::logic_error("invalid matrix multiply");

    //create result matrix
    gMatrix<T> r(ysz(),mRHS.xsz());

    for(int i = 0; i < r.xsz(); i++)
        for(int j = 0; j < r.ysz(); j++)
        {
            r.element(i,j) = 0;
            for(int k = 0; k < mRHS.ysz(); k++)
            {
                r.element(i,j) += get(k,j) * mRHS.get(i,k);
            }
        }

    return r;
}

template<typename T>
gMatrix<T> & gMatrix<T>::operator+=(const gMatrix<T> & mRHS)
{
    if(!samesz(mRHS))
        throw std::runtime_error("invalid matrix add");

    for(int i = 0; i < xsz(); i++)
        for(int j = 0; j < ysz(); j++)
            element(i,j) += mRHS.get(i,j);

    return *this;
}

template<typename T>
gMatrix<T> gMatrix<T>::operator+(const gMatrix<T> & mRHS) const
{
    gMatrix<T> result = *this;
    result += mRHS;
    return result;
}

template<typename T>
gMatrix<T> & gMatrix<T>::operator-=(const gMatrix<T> & mRHS)
{
    *this += -mRHS;

    return *this;
}

template<typename T>
gMatrix<T> gMatrix<T>::operator-(const gMatrix<T> & mRHS) const
{
    gMatrix<T> result = *this;
    result -= mRHS;
    return result;
}

template<typename T>
gMatrix<T> gMatrix<T>::operator-() const
{
    gMatrix<T> result = *this;
    result = result * -1;
    return result;
}

template<typename T>
T gMatrix<T>::get(const int i, const int j) const
{
    if(valid(i,j))
        return m_data[i][j];
    else
        throw std::logic_error("invalid access");
}

template<typename T>
gMatrix<T> gMatrix<T>::transpose() const
{
    gMatrix<T> result(ysz(), xsz());

    for(int i = 0; i < result.xsz(); i++)
        for(int j = 0; j < result.ysz(); j++)
            result.element(i,j) = get(j,i);
    
    return result;
}

template<class T, class R>
gMatrix<R> operator*(const T LHS, const gMatrix<R> & mRHS)
{
    gMatrix<R> res = mRHS;
    for(int i = 0; i < res.xsz(); i++)
        for(int j = 0; j < res.ysz(); j++)
            res.element(i,j) *= LHS;

    return res;
}

template<class T>
ostream& operator<<(ostream& os, const gMatrix<T> & mRHS)
{
    for(int j = 0; j < mRHS.ysz(); j++)
    {
        for(int i = 0; i < mRHS.xsz(); i++)
            os << mRHS.get(i,j) << ", ";
        os << endl;
    }

    return os;
}

template<typename T>
gMatrix<T>::gMatrix(const int mi, const int mj, const T data[])
{
    m_xsz = 0;
    m_ysz = 0;
    resize(mi,mj);

    int count = 0; //iterate throuh data array

    for(int j = 0; j < ysz(); j++)
        for(int i = 0; i < xsz(); i++)
        {
            element(i,j) = data[count];
            count++;
        }


}

template<typename T>
gVector<T> gMatrix<T>::solve(const gVector<T>& vRHS) const
{
    gnorm fn;

    if(xsz() != ysz() || xsz() != vRHS.size())
        throw std::logic_error("invalid matrix size for solve");
    if(!diagdom())
        throw std::logic_error("matrix for solve is not diag dom");

    const int n = xsz(); //size of square matrix, and soln vector

    const gVector<T> b = vRHS; 

    gVector<T> x(n); //used to build solution

    for(int i = 0; i < x.size(); i++)
        x.element(i) = 1;

    gVector<T> xp(x); //stores previous solution

    T diff; //percent difference between xp and p

    gVector<T> c(n); //intermediate value for each iteration

    int k = 0; //tracks total number of iterations

    do
    {
        //determine new offset vector
        for(int i = 0; i < n; i++)
        {
            c[i] = 0;
            for(int j = 0; j < n; j++)
            {
                if(i!=j)
                    c[i] += get(i,j) * x.get(j);
            }
        }
        //update xp with x
        xp = x;

        //apply offset to x
        for(int i = 0; i < n; i++)
        {
            x[i] = (b.get(i) - c.get(i)) / get(i,i);
        }

        //if norm of xp is nonzero, then calculate % diff
        if(fn(xp) != 0)
            diff = fn(x) / fn(xp);
        else
        {
            //if both are zero, % diff is zero
            if(fn(x) == 0)
                diff = 0;
            else
                diff = fn(x);
            //otherwise set to mag of x
        }

        k++;
    } while( diff > MIN_DIFF && k < MAX_ITERS);
    //while the max # of iterations has not been reached and
    //the minimum % diff if norm per iteration has not been reached

    return x;
}


template<class T>
gVector<T> operator*(const gMatrix<T> & mLHS, const gVector<T> & vRHS)
{
    gVector<T> r(vRHS.size());
    
    if(mLHS.ysz() != vRHS.size())
        throw std::logic_error("invalid matrix vector multiplication");

    for(int j = 0; j < mLHS.ysz(); j++)
    {
        r.element(j) = 0;
        for(int i = 0; i < mLHS.xsz(); i++)
        {
            r.element(j) += mLHS.get(i,j) * vRHS.get(i);
        }
    }

    return r;
}

template<class T>
istream& operator>>(istream& is, gMatrix<T>& mRHS)
{
    //total number of elements in matrix
    int dsz = mRHS.xsz() * mRHS.ysz();
    T d[dsz];

    //input into data array all elements
    for(int i = 0; i < dsz; i++)
        is >> d[i];

    //build matrix with sequence of elements and dimeinsions
    gMatrix<T> build(mRHS.xsz(), mRHS.ysz(), d);

    mRHS = build;

    return is;
}

template<>
istream& operator>>(istream& is, gMatrix<int>& mRHS)
{
    //total number of elements in matrix
    int dsz = mRHS.xsz() * mRHS.ysz(); 

    int d[dsz];
    //data array to store elements
    
    //array to store tokens
    string* input = new string[dsz];

    //read in all tokens
    for(int i = 0; i < dsz; i++)
        is >> input[i];

    //convert all tokens to data
    for(int i = 0; i < dsz; i++)
        d[i] = stoi(input[i]);

    //build array from data sequence
    gMatrix<int> build(mRHS.xsz(), mRHS.ysz(), d);

    mRHS = build;

    delete [] input;

    return is;
}

template<>
istream& operator>>(istream& is, gMatrix<float>& mRHS)
{
    //total number of elements in matrix
    int dsz = mRHS.xsz() * mRHS.ysz();

    float d[dsz];
    //data array to store elements
    
    //array to store tokens
    string* input = new string[dsz];

    //read in all tokens
    for(int i = 0; i < dsz; i++)
        is >> input[i];

    //convert all tokens to data
    for(int i = 0; i < dsz; i++)
        d[i] = stof(input[i]);

    //build array from data sequenc
    gMatrix<float> build(mRHS.xsz(), mRHS.ysz(), d);

    mRHS = build;

    delete [] input;

    return is;
}

template<typename T>
void fileInput(const string& fname, gMatrix<T>& data)
{
    //create and open file stream
    ifstream fi;

    fi.open(fname);

    if(!fi.is_open())
        throw std::runtime_error("invalid file access");

    //pass to stream based function
    fileInput(fi, data);

    return;
}

template<typename T>
void fileInput(istream &fi, gMatrix<T>& data)
{
    int n;
    stringstream ss;

    //read in square dimensions
    fi >> n;

    //set dimension
    gMatrix<T> build(n,n);

    //try to read in matrix
    try{
        fi >> build;
    }
    catch(const std::invalid_argument& e)
    {
        ss << "# invalid input matrix";
        throw std::runtime_error(ss.str());
        
    }

    data = build;

    return;
}

template<class T>
void fileInput(const string& fname, gMatrix<T>& data, gVector<T>& mv)
{
    //create and open filestream
    ifstream fi;

    fi.open(fname);

    if(!fi.is_open())
        throw std::runtime_error("invalid file access");

    //input matrix from file
    fileInput(fi, data);

    //resize vector with matrix size from file
    mv.resize(data.xsz());

    //read in vector
    fi >> mv;

    return;
}

template<typename T>
bool gMatrix<T>::diagdom() const
{
    gVector<T> rs(xsz());
    bool result = true;

    if(xsz() != ysz())
        throw std::logic_error("invalid size for diag dominant");

    //sum all rows into vector, excluding diagional values
    for(int j = 0; j < ysz(); j++)
    {
        rs.element(j) = 0;
        for(int i = 0; i < xsz(); i++)
        {
            if(i != j)
                rs.element(j) += abs(get(i,j));
        }

    }

    //compare diagional values to sum
    for(int j = 0; j < xsz(); j++)
    {
        result = result && (abs(get(j,j)) >= rs.get(j));
    }

    return result;
}

template<typename T>
gMatrix<T>::gMatrix(gMatrix<T>&& mRHS)
{
    m_data = mRHS.m_data;
    m_xsz = mRHS.xsz();
    m_ysz = mRHS.ysz();
    mRHS.m_data = nullptr;
    mRHS.m_xsz = 0;
    mRHS.m_ysz = 0;
}