#include <boost/test/included/unit_test.hpp>
#include <sstream>
#include <fstream>

#include "epsilon.h"
#include "gMatrix.h"

using namespace std;


BOOST_AUTO_TEST_SUITE( gm_internal )

BOOST_AUTO_TEST_CASE( gm_construct )
{
	gMatrix<int> t1(3,4);

	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 4; j++)
        {
            t1.element(i,j) = i*j;
		    BOOST_CHECK_EQUAL(t1.get(i,j), i*j);
        }
	}

    BOOST_CHECK_EQUAL(t1.get(2,2), 4);
    BOOST_CHECK_EQUAL(t1.get(1,0), 0);

    BOOST_CHECK_EQUAL(t1.xsz(), 3);
    BOOST_CHECK_EQUAL(t1.ysz(), 4);

	gMatrix<int> t3(t1);
	t1.resize(1,2); //ensure deep copy?
    BOOST_CHECK_EQUAL(t1.xsz(), 1);
    BOOST_CHECK_EQUAL(t1.ysz(), 2);

    BOOST_CHECK_EQUAL(t3.xsz(), 3);
    BOOST_CHECK_EQUAL(t3.ysz(), 4);

	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 4; j++)
        {
		    BOOST_CHECK_EQUAL(t3.get(i,j), i*j);
        }
	}
}

BOOST_AUTO_TEST_CASE( gm_assign )
{
	int d1[] = {1, 2, 3, 4, 5, 6};

    gMatrix<int> t1(3,2,d1);
	gMatrix<int> t2;

	t2 = t1;

    int count = 0;
    BOOST_REQUIRE_EQUAL(t2.xsz(), 3);
    BOOST_REQUIRE_EQUAL(t2.ysz(), 2);
	for(int j = 0; j < t2.ysz(); j++)
	{
		for(int i = 0; i < t2.xsz(); i++)
        {
            BOOST_CHECK_EQUAL(t2.get(i,j), d1[count]);
            count++;
        }
	}

}

BOOST_AUTO_TEST_CASE( gm_math )
{
    int d1[] = {1, 2, 3, 4, 5, 6};
    int d2[] = {7, 8, 9, 10, 11, 12};
    int r1[] = {8, 10, 12, 14, 16, 18};
    int r2[] = {6, 6, 6, 6, 6, 6};
    
    gMatrix<int> t1(3,2,d1);
    gMatrix<int> t2(3,2,d2);
    gMatrix<int> t3, t4;

    t3 = t1 + t2;
    t4 = t2 - t1;

    BOOST_REQUIRE(t1.samesz(t3));
    BOOST_REQUIRE(t1.samesz(t4));

    int count = 0;
    for(int j = 0; j < 2; j++)
	{
		for(int i = 0; i < 3; i++)
        {
            BOOST_CHECK_EQUAL(t3.get(i,j), r1[count]);
            BOOST_CHECK_EQUAL(t4.get(i,j), r2[count]);
            count++;
        }
	}

}

BOOST_AUTO_TEST_CASE( gm_scalar_math )
{
    int d1[] = {1, 2, 3, 4, 5, 6};
    
    gMatrix<int> t1(3,2,d1);
    gMatrix<int> t3;
    
    t3 = 2 * t1;

    for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 2; j++)
        {
            BOOST_CHECK_EQUAL(t3.get(i,j), t1.get(i,j)*2);
        }
	}


}

BOOST_AUTO_TEST_CASE( gm_matrix_product )
{
    const float d1 [] = {1, 2, 3, 4, 5, 6};
    const float d2 [] = {7, 8, 9, 10, 11, 12};
    const float d3 [] = {58, 64, 139, 154};

    gMatrix<float> m1(3,2,d1);
    gMatrix<float> m2(2,3,d2);
    gMatrix<float> m3(2,2,d3);
    gMatrix<float> m4;

    m4 = m1 * m2;

    BOOST_REQUIRE_EQUAL(m4.xsz(),m3.xsz());
    BOOST_REQUIRE_EQUAL(m4.ysz(),m3.ysz());

    for(int i = 0; i < m4.xsz(); i++)
        for(int j = 0; j < m4.ysz(); j++)
            BOOST_CHECK_EQUAL(m4.get(i,j),m3.get(i,j));

}

BOOST_AUTO_TEST_CASE( gm_vector_product )
{
    const float d1 [] = {1, 2, 3, 4};
    const float d2 [] = {2, 3};
    const float d3 [] = {8, 18};

    gMatrix<float> m1 (2,2,d1);
    gVector<float> v1 (2, d2);
    gVector<float> v2 (2,d3);

    gVector<float> r1 = m1 * v1;

    BOOST_REQUIRE_EQUAL(r1.size(), v2.size());

    for(int i = 0; i < r1.size(); i++)
        BOOST_CHECK_EQUAL(r1.get(i), v2.get(i));



}

BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE( gm_input )

BOOST_AUTO_TEST_CASE( gm_input_single )
{
    gMatrix<int> d3;

    const int d1 [] = {11,  1,  -1,  2,  -1,  0,  0,  0,  1,  -2,
                        2,  -25,  0,  0,  5,  -1,  3,  -6,  -2,  3,
                        0,  1,  5,  0,  0,  0,  0,  1,  0,  0,
                        -1,  -1,  2,  -15,  1,  -2,  0,  1,  -1,  -1,
                        3,  -1,  -1,  0,  21,  -4,  -1,  1,  0,  0,
                        -4,  2,  -1,  0,  0,  17,  -2,  1,  -3,  1,
                        0,  0,  0,  0,  0,  1,  -8,  -1,  0,  4,
                        0,  1,  6,  -1,  -1,  0,  2,  19,  -1,  0,
                        1,  0,  0,  1,  1,  -3,  -2,  0,  10,  1,
                        -1,  -1,  -1,  1,  1,  -1,  -1,  1,  1,  -11};

    fileInput("data/t1.txt", d3);

    gMatrix<int> d2(10,10,d1);

    BOOST_REQUIRE_EQUAL(d3.xsz(), d2.xsz());
    BOOST_REQUIRE_EQUAL(d3.ysz(), d2.ysz());

    for(int i = 0; i < d3.xsz(); i++)
        for(int j = 0; j < d3.ysz(); j++)
            BOOST_CHECK_EQUAL(d3.get(i,j),d2.get(i,j));


}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE( gm_solve )

BOOST_AUTO_TEST_CASE( gm_jacobi )
{
    const auto PERCENT_TOLERANCE = 0.1;
    const float d1 [] = {10, -1,  2,  0,
                         -1, 11, -1,  3,
                         2, -1, 10, -1,
                         0,  3, -1,  8 };
    const float d2 [] = {6, 25, -11, 15};
    const float d4 [] = {1,2,-1,1};

    gMatrix<float> m1(4,4, d1);
    gVector<float> v1(4, d2);
    gVector<float> v2 = m1.solve(v1);

    BOOST_REQUIRE_EQUAL(v2.size(),4);
    for(int i = 0; i < v2.size(); i++)
    {
        BOOST_CHECK_CLOSE(v2[i],d4[i], PERCENT_TOLERANCE);
    }

}

BOOST_AUTO_TEST_CASE( gm_file_solve )
{
    gMatrix<float> m1;
    fileInput("data/t1.txt", m1);
    const auto PERCENT_TOLERANCE = 0.1;

    const float d2 [] = {-9,  -23,  15,  -10,  -87,  16,  50,  -13,  -21,  -124};
    const float d3 [] = {1, 2, 3, 0, -4, 0, -1, -2, -3, 10};

    gVector<float> v1(10, d2);
    gVector<float> v2 = m1.solve(v1);

    BOOST_REQUIRE_EQUAL(v2.size(),10);
    for(int i = 0; i < v2.size(); i++)
    {
        BOOST_CHECK_CLOSE(v2.get(i), d3[i], PERCENT_TOLERANCE );
    }

}

BOOST_AUTO_TEST_SUITE_END()