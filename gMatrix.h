//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW5
//gMatrix.hpp, definition of matrix class

#pragma once

#include "gVector.h"
#include "gnorm.h"

//Problems

//one day....
//TODO: jacobi works? haha

//Housekeeping
//TODO: UML


using namespace std;

const int MATRIX_DEFAULT_SIZE = 3;

template<class T>
class gMatrix
{
    private:
        T **m_data;
        int m_xsz;
        int m_ysz;

    public:
        //constructs empty matrix with xsz by ysz dimension
        //pre: xsz & ysz >= 0
        //pst: memorry allocated
        gMatrix(const int xsz = 3, const int ysz = 3);

        //construct matrix with same size and data as mRHS
        //pre: operator= defined for T
        //pst: same size and deep data copy from mRHS
        gMatrix(const gMatrix<T>& mRHS);

        //move constructor for mRHS
        //pre: operator= defined for T
        //pst: mRHS size set to zero, data set to nullptr
        //     new object retains mRHS m_data pointer
        //     size matches vRHS
        gMatrix(gMatrix<T>&& mRHS);

        //construct from array
        //pre: data is size of mi * mj
        //pst: matrix size is now mi x mj
        //      elements correspond to data[]
        gMatrix(const int mi, const int mj, const T data[]);

        //destructor
        //pre:
        //pst: dynamic data deallocated
        ~gMatrix() { resize(0,0); }

        //confirm same size
        //pre:
        //pst: returns true iff calling object and mRHS have same dims
        bool samesz(const gMatrix<T> & mRHS) const;

        //confirm valid location
        //pre:
        //pst: true iff i,j is valid index into matrix
        bool valid(const int i, const int j) const;

        //diagionally dominant test
        //pre: calling is square matrix
        //pst: returns true if diag dominant
        bool diagdom() const;

        //x dimension
        //pre:
        //pst: returns x dimension
        int xsz() const {return m_xsz;}

        //y dimension
        //pre:
        //pst: returns y dimension
        int ysz() const {return m_ysz;}

        //deletes and reallocates m_data
        //pre: i,j >= 0
        //pst: data cleared, reallocated to input size
        //     xsz and ysz updated to new size
        void resize(const int i, const int j);

        //copy assignment
        //pre:
        //pst: deep copy of data of mRHS
        //     size of calling object updated to size of mRHS
        gMatrix<T> & operator=(const gMatrix<T> & mRHS);

        //matrix multiplication
        //pre: size of calling object and mRHS compatible
        //      operator*,+,+= defined for T
        //pst: returns matrix multiplication result
        gMatrix<T> operator*(const gMatrix<T> & mRHS) const;

        //matrix multiplication assignment
        //pre: size of calling object and mRHS compatible
        //      operator*,+,+= defined for T
        //pst: updates calling with matrix mult result
        gMatrix<T> & operator*=(const gMatrix<T> & mRHS);

        //matrix add
        //pre: size of calling object and mRHS compatible
        //      operator+ defined for T
        //pst: returns matrix add result
        gMatrix<T> operator+(const gMatrix<T> & mRHS) const;

        //matrix add assignment
        //pre: size of calling object and mRHS compatible
        //      operator+ defined for T
        //pst: updates calling with matrix add result
        gMatrix<T> & operator+=(const gMatrix<T> & mRHS);
        
        //matrix subtract
        //pre: size of calling object and mRHS compatible
        //      operator*,+ defined for T
        //pst: returns matrix subtract result
        gMatrix<T> operator-(const gMatrix<T> & mRHS) const;

        //matrix subtract assignment
        //pre: size of calling object and mRHS compatible
        //      operator*,+ defined for T
        //pst: updates calling with matrix subtract result
        gMatrix<T> & operator-=(const gMatrix<T> & mRHS);
        
        //matrix negative
        //pre: operator* defined for T
        //pst: returns copy of matrix scaled with -1 
        gMatrix<T> operator-() const;

        //element access
        //pre: i,j valid location, valid(i,j) == true
        //pst: returns COPY to i,jth element in matrix
        T get(const int i, const int j) const;

        //element access
        //pre: i,j valid location, valid(i,j) == true
        //pst: returns REFERENCE to i,jth element in matrix
        T& element(const int i, const int j);

        //transpose
        //pre: 
        //pst: returns matrix transpose        
        gMatrix<T> transpose() const;

        //solve with jacobinan iteration
        //pre: matrix is square, diagionally dominant
        //      operator*,/,+,+=,- defined for T
        //pst: returns solution vector
        gVector<T> solve(const gVector<T> & vRHS) const;
};

//scalar multiplication
//each element of  mRHS operator* with i
//pre: operator*(multiplication) defined for T and R
//pst: new object returned with result of scalar multiplication
template<class T, class R>
gMatrix<R> operator*(const T LHS, const gMatrix<R> & mRHS);

//scalar multiplication
//each element of  mRHS operator* with i
//pre: operator*(multiplication) defined for T and R
//pst: new object returned with result of scalar multiplication
template<class T, class R>
gMatrix<R> operator*(const gMatrix<R> & mRHS, const T i)
{
    return i * mRHS;
}

//vector multiplication
//pre: size of matrix and vector compatible
//      operator* defined for T
//pst: new object returned with result of multiplication
template<class T>
gVector<T> operator*(const gMatrix<T> & mLHS, const gVector<T> & vRHS);

//~~pretty print~~
//pre:
//pst: outputs operator<< for each element in row
//     followed by endl, then next row
//     prints nothing if either dimension is 0
template<class T>
ostream& operator<<(ostream& os, const gMatrix<T> & mRHS);


//input into matrix
//input checking provided only for float and int types for T
//pre: dimension set to desired number of elements
//pst: xsz() * ysz() elements read in and mRHS upated
template<class T>
istream& operator>>(istream& is, const gMatrix<T> & mRHS);

//input from file
//pre:  fname references file with format...
//      first line of file is dimension of matrix, assumed square
//      following, one row per line, elements seperated with
//          whitespace
//      operator>> defined for gMatrix
//pst:  matrix contains the data represented in file
template<class T>
void fileInput(const string& fname, gMatrix<T>& data);


//input from file matrix & vector
//pre:  fname references file with format...
//      first line of data is dimension of matrix, assumed square
//      following, one row per line, elements seperated with
//          whitespace
//      THEN, gVector on last line.
//          elements seperated with whitespace
//          same size as matrix xsz() from first line
//      operator>> defined for gMatrix and gVector
//pst:  matrix contains the data represented in file
//      vector contains the data represented in the file
template<class T>
void fileInput(const string& fname, gMatrix<T>& data, gVector<T>& mv);

//input from stream just matrix 
//pre:  fname references file with format...
//      first line of data is dimension of matrix, assumed square
//      following, one row per line, elements seperated with
//          whitespace
//      operator>> defined for gMatrix 
//pst:  matrix contains the data represented in steam
template<class T>
void fileInput(istream &fi, gMatrix<T>& data);

#include "gMatrix.hpp"