//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW5
//gnorm.h, definition of norm class

#pragma once

#include "gVector.h"

using namespace std;

class gnorm
{
public:
    //magnitude/norm of vector
    //pre: operator* defined for T
    //pst: returns magnitude/norm of vector
    template<typename T>
    T operator()(const gVector<T>& v) {return v.mag();}
};
